# Start out with the same node image
FROM node:8.9.1-alpine

# Copy all local files to VM
# Hint: add a .dockerignore file to ignore certain files on COPY
COPY . /usr/project

# Set the working directory
WORKDIR /usr/project

CMD ls

# Retrieve all dependencies
RUN npm install

# Expose port 3000
EXPOSE 3000

# What gets called when VM launches
CMD node server.js
