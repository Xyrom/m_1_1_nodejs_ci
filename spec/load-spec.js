var loadModule = require('../load');


describe ('Test suite for checking userName', function () {
	it('should accept the given usernames format', function () {
		var result = loadModule.userName("username");

		expect(result).toBe(true);
	});

	it('shouldn\'t accept the given usernames format', function () {
		var result = loadModule.userName("Username");

		expect(result).toBe(false);
	});

});
