/*Domokos Cristina Edina dcim1510 521 Feladat 1.1
Gyakorlat 1.1 HTML, CSS és NodeJS-t használva készítsünk egy bejelentkez˝o (login) oldalt, mely lehet˝oséget biztosít: • A már regisztrált felhasználók bejelenkezésére • Új felhasználók regisztrálására (a II. a) pontban megadott feltételek alapján) • Aregisztráltfelhasználókadataittároljukaszerveren(adatbázisbanvagyállományban). • AII.b)pontbanmegadottfeladatnakmegfelel˝ooldalelérésecsaksikeresbejelentkezés után legyen lehetséges. • Legyen lehet˝oség a kijelentkezésre • A regisztrálásnál megadott adatok formai ellen˝orzését kliens oldalon, logikai ellen˝orzését (pl. egyedi felhasználónév, múltbéli születési dátum stb) szerver oldalon végezzük! • A hibás adatbevitelt jelezzük megfelel˝oen! 
Feladat 1.1
A Regisztrációhoz szükséges adatok: • felhasználónév-csakkisbetu˝ket,számjegyeketvagy"_"karakterttartalmazhat(max. 10 karakter) • felhasználó civil neve (vezetéknév, keresztnév) • felhasználó foglalkozása (nem kötelez˝ o) • jelszó (min. 6, max 10 karaktert tartalmazzon, legyen benne kis- és nagybetu˝ is) • e-mail cím Afoglalkozástkivéve,azösszestöbbiadatotkötelez˝olegyenmegadniasikeresregisztrálás érdekében. A feltételeket írjuk is ki (eleve vagy csak sikertelen próbálkozás esetén), hogy a felhasználó tudja mit hogyan kell megadnia ahhoz, hogy sikeresen regisztrálhasson.
B Értelmez˝o szótár készítés: Bármelyik felhasználó rákereshet (reguláris kifejezésre illeszkedo˝) szavakra (anélkül, hogy bejelentkezne),ésválaszulhivatkozás-listátkapazilleszked˝oszavakkal(vagyhibaüzenetet, ha nincs találat) A megfelel˝o hivatkozásra kattintva a szóhoz fu˝zött magyarázat is megjelenik. Regisztáltésbejelentkezettfelhasználók: • ugyanúgy rákereshetnek szavakra, viszont ˝ok azt is látják, hogy melyik megjegyzést milyen azonosítójú illetve nevu˝ felhasználó írt be • új szavakat vihetnek be, meg kell adni a szót, és a hozzá fu˝zött magyarázatot (már létez˝o szóhoz nem lehet újabb magyarázatot fu˝zni)

*/

function registration(){
	 window.location='reg';
}

module.exports.userName = function(s){
	var letterNumber = /^[\a-z\d_.-]+$/;  
	if(s.length<=10 && s.match(letterNumber)){  
	   return true;  
	  }  
	else  
	  {   
	   return false;   
	  }
}

function jelszo(s){

	var re = /^(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{6,10}$/;
	if(s.match(re)){
		 return true;  
	  }  
	else  
	  {      
	   return false;   
	  }
	
}

function ellenorzes(form){
	var s = form.getElementsByTagName('input');
	var ok=0;
	
	if(s[0].value!=""){
		if(userName(s[0].value)){
			ok = ok+1;
			document.getElementById("uname").innerHTML="";
		}else{
			document.getElementById("uname").innerHTML="Rossz formatum!";
		}
	
	
	if(s[1].value!=""){
		ok = ok+1;
		document.getElementById("fname").innerHTML="";
	}
	
	
	if(s[2].value!=""){
		ok = ok+1;
		document.getElementById("lname").innerHTML="";
	}
	
	
	if(s[5].value!=""){
		ok = ok+1;
		document.getElementById("emaile").innerHTML="";
	}
	
	
	if(s[4].value!=""){
		if(jelszo(s[4].value)){
			ok = ok + 1;
			document.getElementById("passw").innerHTML="";
		}else{
			document.getElementById("passw").innerHTML="Rossz formatum!";
		}
	
	
	if(ok==5){
		document.getElementById("gomb").className="show";
	}
	else{
		document.getElementById("gomb").className="hide";
	}
	
}

}
}