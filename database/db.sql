create database registers 
use database registers
CREATE TABLE  registers.users (
  id INT(11) NOT NULL AUTO_INCREMENT,
  userName VARCHAR(45),
  firstName VARCHAR(45),
  lastName VARCHAR(45),
  job VARCHAR(45),
  pass VARCHAR(45),
  email VARCHAR(45),
  PRIMARY KEY (id)
);

CREATE TABLE registers.registry (
    rid INT NOT NULL AUTO_INCREMENT,
    id INT,
    word VARCHAR(20),
    exp VARCHAR(100),
    PRIMARY KEY (rid),
    FOREIGN KEY (id) REFERENCES users(id)
)


