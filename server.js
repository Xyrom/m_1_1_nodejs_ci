/*Domokos Cristina Edina dcim1510 521 Feladat 1.1
Gyakorlat 1.1 HTML, CSS és NodeJS-t használva készítsünk egy bejelentkez˝o (login) oldalt, mely lehet˝oséget biztosít: • A már regisztrált felhasználók bejelenkezésére • Új felhasználók regisztrálására (a II. a) pontban megadott feltételek alapján) • Aregisztráltfelhasználókadataittároljukaszerveren(adatbázisbanvagyállományban). • AII.b)pontbanmegadottfeladatnakmegfelel˝ooldalelérésecsaksikeresbejelentkezés után legyen lehetséges. • Legyen lehet˝oség a kijelentkezésre • A regisztrálásnál megadott adatok formai ellen˝orzését kliens oldalon, logikai ellen˝orzését (pl. egyedi felhasználónév, múltbéli születési dátum stb) szerver oldalon végezzük! • A hibás adatbevitelt jelezzük megfelel˝oen! 
Feladat 1.1
A Regisztrációhoz szükséges adatok: • felhasználónév-csakkisbetu˝ket,számjegyeketvagy"_"karakterttartalmazhat(max. 10 karakter) • felhasználó civil neve (vezetéknév, keresztnév) • felhasználó foglalkozása (nem kötelez˝ o) • jelszó (min. 6, max 10 karaktert tartalmazzon, legyen benne kis- és nagybetu˝ is) • e-mail cím Afoglalkozástkivéve,azösszestöbbiadatotkötelez˝olegyenmegadniasikeresregisztrálás érdekében. A feltételeket írjuk is ki (eleve vagy csak sikertelen próbálkozás esetén), hogy a felhasználó tudja mit hogyan kell megadnia ahhoz, hogy sikeresen regisztrálhasson.
B Értelmez˝o szótár készítés: Bármelyik felhasználó rákereshet (reguláris kifejezésre illeszkedo˝) szavakra (anélkül, hogy bejelentkezne),ésválaszulhivatkozás-listátkapazilleszked˝oszavakkal(vagyhibaüzenetet, ha nincs találat) A megfelel˝o hivatkozásra kattintva a szóhoz fu˝zött magyarázat is megjelenik. Regisztáltésbejelentkezettfelhasználók: • ugyanúgy rákereshetnek szavakra, viszont ˝ok azt is látják, hogy melyik megjegyzést milyen azonosítójú illetve nevu˝ felhasználó írt be • új szavakat vihetnek be, meg kell adni a szót, és a hozzá fu˝zött magyarázatot (már létez˝o szóhoz nem lehet újabb magyarázatot fu˝zni)

*/

var http = require('http');
var path = require('path');
var express = require('express');
var logger = require('morgan');
var util = require('util');
var fs = require('fs');
var cookieParser = require('cookie-parser');
//a szerver létrehozása 
//var app = express();

var formidable = require('formidable');
var util = require('util');
//var sql = require('mssql');
var mysql = require('mysql')

var pool  = mysql.createPool({
  connectionLimit : 10,
  host            : process.env.MYSQL_HOST,
  user            : process.env.MYSQL_USER,
  password        : process.env.MYSQL_PASSWORD,
  database 		  : process.env.MYSQL_DATABASE,
  port            : 3306
});


    

var app = express();
var server = http.createServer(app);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.locals.pretty = true;

app.use(logger('dev'));


app.use(cookieParser());


	app.get('/index', function(request, response){
    response.sendFile(__dirname +'/index.html');
	
})

app.get('/reg', function(request, response){
    response.sendFile(__dirname +'/reg.html');
	
})

app.get('/regbad', function(request, response){
	
   fs.readFile('reg.html',function (err, data){
        response.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
		response.write('<script>alert("Mar letezik ilyen felhasznalo!")</script>');
		response.write(data);
        response.end();
	
		})
})

app.get('/indexbad', function(request, response){

		response.sendFile(__dirname +'/indexbad.html');
})

app.get('/lab3.css', function(request, response){
    response.sendFile(__dirname +'/lab3.css');
	
})
app.get('/load.js', function(request, response){
    response.sendFile(__dirname +'/load.js');
	
})

app.get('/tabla.js', function(request, response){
    response.sendFile(__dirname +'/tabla.js');
	
})

app.route('/logout')
	.get( function( req, resp){
		resp.clearCookie('session');
		resp.redirect('/search');
		
	})

app.route('/search')
	.get( function( req, resp){
		resp.render('nonuser', function( err, html) {
			if ( err ) 
				console.log('render error:'+err.toString());
			else
				resp.send(html);
		})
	})
	

	
app.route('/searching_non')
	.post( function( req, resp){
		var form = new formidable.IncomingForm();
		var dynamicdata;
		form.parse(req, function( err, fields) {
		console.log(util.inspect({ fields: fields}));
		var wordName = '%'+fields.word+'%';
		
		var querry1 = "SELECT word, exp FROM registry WHERE word LIKE ?";
			pool.query(querry1, wordName, function(err, rows, fields){
						if (err ) 
								console.log('nem keres'+err.message);
						else{ 
								console.log('megvan, tovabb:');
								if(rows==""){
									resp.render('nonuser_search',{message: 'Nincs ilyen szo!'}, function( err, html) {
										if ( err ) 
											console.log('render error:'+err.toString());
										else
											resp.send(html);
									})	
								}
							
								else{
									var ss=[];
									var dynamicdata = [];
									var k = 0;
									for (var i = 0; i < rows.length; i++) {
										dynamicdata[k]=rows[i].word;
										dynamicdata[k+1] = "|";
										k = k+2;
								}
								k = 0;
								for (var i = 0; i < rows.length; i++) {
										ss[k]=rows[i].exp;
										ss[k+1] = "|";
										k = k+2;
								}
								
									resp.render('nonuser_search',{message: dynamicdata, mess: ss}, function( err, html) {
										if ( err ) 
											console.log('render error:'+err.toString());
										else
											resp.send(html);
									})
									
								}
						}
					})
	})
})

app.route('/usersearch')
	.get( function( req, resp){
		var ok = 0;
	     for(var cookie  in req.cookies){
			ok = ok +1;
			console.log(req.cookies[cookie]);}
			
								 
		
		if(ok>0){
		resp.render('user', function( err, html) {
			if ( err ) 
				console.log('render error:'+err.toString());
			else
				resp.send(html);
		})
		}else{
			resp.redirect('/search');
		}
	})
	
app.route('/searching_user')
	.post( function( req, resp){
		var form = new formidable.IncomingForm();
		var dynamicdata;
		form.parse(req, function( err, fields) {
		console.log(util.inspect({ fields: fields}));
		var wordName = '%'+fields.word+'%';;
		
		var querry1 = "SELECT word, exp, userName, firstName, lastName FROM registry a, users b WHERE a.word LIKE ? AND b.id=a.id";
			pool.query(querry1, wordName, function(err, rows, fields){
						if (err ) 
								console.log('Hiba'+err.message);
						else{ 
								if(rows==""){
									resp.render('user_search',{message: 'Nincs ilyen szo!'}, function( err, html) {
										if ( err ) 
											console.log('render error:'+err.toString());
										else
											resp.send(html);
									})	
								}
							
								else{
									
									var s=[];
									var ss=[];
									var sss=[];
									var k =0;
									for (var i = 0; i < rows.length; i++) {
									    s[k]=rows[i].word;
										s[k+1]="|";
										k=k+2;
								}
								k=0;
								for (var i = 0; i < rows.length; i++) {
									    ss[k]=rows[i].exp;
										ss[k+1]="|";
										k=k+2;
								}
								k=0;
								for (var i = 0; i < rows.length; i++) {
										sss[k]=rows[i].userName+"("+rows[i].firstName+" "+rows[i].lastName+")";
										sss[k+1]="|";
										k=k+2;
								}
									
									resp.render('user_search',{message: s, mess: ss, me: sss}, function( err, html) {
										if ( err ) 
											console.log('render error:'+err.toString());
										else
											resp.send(html);
									})
									
								}
						}
					})
	})
})
	
app.route('/add')
	.get( function( req, resp){
		resp.render('add', function( err, html) {
			if ( err ) 
				console.log('render error:'+err.toString());
			else
				resp.send(html);
		})
	})
	
app.route('/save')
	.post( function( req, response){
		var form = new formidable.IncomingForm();
			
		form.parse(req, function( err, fields) {
		console.log(util.inspect({ fields: fields}));
			var word = fields.word;
			var exp = fields.exp;
			var user = req.cookies['session'];
			console.log(user);
			if(word!=""){
				var querry1 = "SELECT rid FROM registry WHERE word = ?";
				var querry3 = "INSERT INTO registry SET ?";
				var querry2 = "SELECT id FROM users WHERE userName=?";
				
					pool.query(querry1, word, function(err, rows, fields){
						if (err) 
								console.log('Hiba'+err.message);
						else{ 
								if(rows==""){
							
							pool.query(querry2, user, function(err, rows, fields){
								var id = rows[0].id;
								
								var pot = {id: id, word: word, exp: exp};
								pool.query(querry3, pot, function(err, rows, fields){
									response.redirect('/usersearch');
								
								})
							})
								}else{								
									response.redirect('/usersearch');
									
								}
						}//else

					})
			}
		
		})
		
	})

app.route('/form')

	.post(  function( request, response ){
			
				var form = new formidable.IncomingForm();
			
				form.parse(request, function( err, fields) {
					
					var userName = fields.username;
					var fName = fields.firstname;
					var lName = fields.lastname;
					var job = fields.job;
					var passw = fields.pass;
					var email = fields.email;
					if(userName!="" && fName!="" && lName!="" && passw!="" && email!=""){
					var querry1 = "SELECT id FROM users WHERE userName= ? ";
					
					var querry2 = "INSERT INTO users SET ?";
					var pot = {userName: userName, firstName: fName, lastName: lName, job: job, pass: passw, email: email };
					
					pool.query(querry1, userName, function(err, rows, fields){
						if (err ) 
								console.log('Hiba'+err.message);
						else{ 
								if(rows==""){
							
							pool.query(querry2, pot, function(err, rows, fields){
								if (err ) 
										console.log('Sikertelen beszuras');
								else 
										console.log('Sikeres beszuras');
										response.redirect('/index');
							
							})
								}else{
									console.log("Mar van ilyhen felhasznalo!");
									
									response.redirect('/regbad');
									
								}
						}//else

					})
								
					}//if
			})

	})

app.route('/login')

	.post(  function( request, response ){
				console.log('received a http request with POST method');
				console.log('parsing form data from the request');
				var form = new formidable.IncomingForm();
				
				
				form.parse(request, function( err, fields) {
					console.log(util.inspect({ fields: fields}));
					
					var userName = fields.username;
					var passw = fields.pass;

					var querry1 = "SELECT id FROM users WHERE userName= ? and pass = ? ";
					
					var querry2 = "INSERT INTO users SET ?";
					
					pool.query(querry1, [userName, passw], function(err, rows, fields){
						if (err ) 
								console.log('Hiba'+err.message);
						else{ 
								
								if(rows==""){
									console.log("rossz jelszo/username");
									response.redirect('/indexbad');
								}else{
									console.log("Sikeres belepes!");
									response.cookie('session',userName);
									response.redirect('usersearch');
									
								}
						}//else

					})

				//	}//if
			})

	});
	

server.listen(3000);